import React, { Component } from 'react'
import '../App.css';
import { Col, Container, Row } from 'react-bootstrap';
import { Cart, ListCategory, MenuComponent, NavbarComponent } from '../components'
import { API_URL } from '../utils/constant'
import axios from 'axios';
import Swal from 'sweetalert2';

export default class Home extends Component {

    constructor(props) {
        super(props)

        this.state = {
            menus: [],
            selectCategories: '',
            keranjangs: []
        }
    }

    componentDidMount() {
        axios
            .get(API_URL + "products")
            .then(res => {
                const menus = res.data;
                this.setState({ menus })
            })
            .catch(error => {
                console.log(error);
            })

        axios
            .get(API_URL + "keranjangs")
            .then(res => {
                const keranjangs = res.data;
                this.setState({ keranjangs })
            })
            .catch(error => {
                console.log(error);
            })
    }

    componentDidUpdate(prevState) {
        if (this.state.keranjangs !== prevState.keranjangs) {

            axios
                .get(API_URL + "keranjangs")
                .then(res => {
                    const keranjangs = res.data;
                    this.setState({ keranjangs })
                })
                .catch(error => {
                    console.log(error);
                })
        }
    }

    changeCategory = (value) => {
        this.setState({
            selectCategories: value,
            menus: []
        })
        axios
            .get(API_URL + "products?category.nama=" + value)
            .then(res => {
                const menus = res.data;
                this.setState({ menus })
            })
            .catch(error => {
                console.log(error);
            })
    }

    masukKeranjang = (value) => {
        console.log("Menu :", value);

        axios
            .get(API_URL + "keranjangs?product.id=" + value.id)
            .then(res => {
                if (res.data.length === 0) {
                    const keranjang = {
                        jumlah: 1,
                        total_harga: value.harga,
                        product: value
                    }
                    axios
                        .post(API_URL + "keranjangs", keranjang)
                        .then(res => {
                            Swal.fire({
                                title: "Berhasil",
                                text: "Produk berhasil ditambahkan",
                                timer: 3000,
                                icon: "success",
                                button: false
                            })
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {

                    const keranjang = {
                        jumlah: res.data[0].jumlah + 1,
                        total_harga: res.data[0].total_harga + value.harga,
                        product: value
                    }

                    axios
                        .put(API_URL + "keranjangs/" + res.data[0].id, keranjang)
                        .then(res => {
                            Swal.fire({
                                title: "Berhasil",
                                text: "Produk berhasil ditambahkan",
                                timer: 3000,
                                icon: "success",
                                button: false
                            })
                        })
                        .catch(error => {
                            console.log(error);
                        })
                }
            })
            .catch(error => {
                console.log(error);
            })
    }

    render() {
        //console.log(this.state.menus);
        const { menus, selectCategories, keranjangs } = this.state
        return (

            <div className="mt-4">
                <Container fluid>
                    <Row>
                        <ListCategory changeCategory={this.changeCategory} selectCategories={selectCategories} />
                        <Col>
                            <h4><strong>Daftar Menu</strong></h4>
                            <hr />
                            <Row>
                                {
                                    menus && menus.map((menu) => (
                                        <MenuComponent
                                            key={menu.id}
                                            menu={menu}
                                            masukKeranjang={this.masukKeranjang}
                                        />
                                    ))}
                            </Row>
                        </Col>
                        <Cart keranjangs={keranjangs} {...this.props} />
                    </Row>
                </Container>
            </div>

        )
    }
}
