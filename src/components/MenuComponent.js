import React from 'react'
import { Button, Card, Col } from 'react-bootstrap'
import { numberWithCommas } from '../utils/utils'

export const MenuComponent = ({ menu,masukKeranjang }) => {
    return (
        <Col md={4} xs={6} className="mb-4">
            <Card className="shadow" onClick={() => masukKeranjang(menu)}>
                <Card.Img variant="top" src={"assets/images/"+menu.category.nama.toLowerCase()+"/"+menu.gambar} />
                <Card.Body>
                    <Card.Title>{menu.nama}</Card.Title>
                    <Card.Text>
                        Rp.{numberWithCommas(menu.harga)}
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    )
}
