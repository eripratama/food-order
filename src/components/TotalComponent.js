import { faCashRegister } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { numberWithCommas } from '../utils/utils';
import axios from "axios";
import { API_URL } from '../utils/constant'

export default class TotalComponent extends Component {

    submitTotalBayar = (totalBayar) => {
        const pesanan = {
            total_bayar: totalBayar,
            menus: this.props.keranjangs
        }
        axios
            .post(API_URL+"pesanans",pesanan)
            .then((res) => {
                this.props.history.push('/finish')
            })
    }
    render() {

        const totalBayar = this.props.keranjangs.reduce(function (result, item) {
            return result + item.total_harga
        }, 0);

        return (
            <div className="fixed-bottom">
                <Row>
                    <Col md={{ span: 3, offset: 9 }} className="px-4">
                        <h4>Total : <strong className="float-end mr-2">{numberWithCommas(totalBayar)}</strong></h4>
                        <div className="d-grid gap-2 mb-2">
                            <Button variant="primary" size="md"
                                onClick={() => this.submitTotalBayar(totalBayar)}
                            >
                                <FontAwesomeIcon icon={faCashRegister} />  <strong>Bayar</strong>
                            </Button>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}
