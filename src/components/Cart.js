import React, { Component } from 'react'
import { Badge, Col, ListGroup, Row } from 'react-bootstrap'
import { numberWithCommas } from '../utils/utils'
import ModalComponent from './ModalComponent'
import TotalComponent from './TotalComponent'

export default class Cart extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             showModal: false,
             keranjangDetail: false,
             jumlah:0,
             keterangan:''
        }
    }

    handleShow = (menuKeranjang) => {
        this.setState({
            showModal:true,
            keranjangDetail:menuKeranjang
        })
    }

    handleClose = () => {
        this.setState({
            showModal:false
        })
    }
    
    render() {
        const { keranjangs } = this.props
        return (
            <Col md={3} mt="4">
                <h4><strong>List Order</strong></h4>
                <hr />
                {keranjangs.length !== 0 && (
                    <ListGroup variant="flush">
                        {keranjangs.map((menuKeranjang) => (
                            <ListGroup.Item key={menuKeranjang.id}
                                onClick={() => this.handleShow(menuKeranjang)}
                            >
                                <Row>
                                    <Col xs={2}>
                                        <h4><Badge variant="info">{menuKeranjang.jumlah}</Badge></h4>
                                    </Col>
                                    <Col>
                                        <h5>{menuKeranjang.product.nama}</h5>
                                        <p>Rp.{numberWithCommas(menuKeranjang.product.harga)}</p>
                                    </Col>
                                    <Col>
                                        <strong className="float-end">Rp.{numberWithCommas(menuKeranjang.total_harga)}</strong>
                                    </Col>
                                </Row>
                            </ListGroup.Item>
                        ))}
                        <ModalComponent handleClose={this.handleClose} {...this.state}/>
                    </ListGroup>
                )}
                <TotalComponent keranjangs={keranjangs} {...this.props}/>
            </Col>
        )
    }
}
