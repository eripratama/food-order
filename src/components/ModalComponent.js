import { faMinus, faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { Col, ListGroup, Row, Badge, Modal, Button, Form } from 'react-bootstrap'
import { numberWithCommas } from '../utils/utils'

const ModalComponent = ({ showModal, handleClose, keranjangDetail }) => {
    //console.log("cek Cart",keranjangItem);
    return (
        <Modal show={showModal} onHide={handleClose} >
            <Modal.Header closeButton>
                <Modal.Title>Update Item</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form> 
                    {/* onSubmit={handleSubmit} */}
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Total Harga :</Form.Label>
                        <p>
                            <strong>
                                Rp. {keranjangDetail.total_harga}
                            </strong>
                        </p>
                    </Form.Group>

                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Jumlah :</Form.Label>
                        <br />
                        <Button variant="primary" size="sm" className="mr-3" >
                            <FontAwesomeIcon icon={faMinus} />
                        </Button>

                        <strong>{keranjangDetail.jumlah}</strong>

                        <Button variant="primary" size="sm" className="ml-3" >
                            <FontAwesomeIcon icon={faPlus} />
                        </Button>
                    </Form.Group>

                    <Form.Group controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Keterangan :</Form.Label>
                        <Form.Control
                            as="textarea"
                            rows="3"
                            name="keterangan"
                            placeholder="Contoh : Pedes, Nasi Setengah"
                            // value={keterangan}
                            // onChange={(event) => changeHandler(event)}
                        />
                    </Form.Group>
                    <Button variant="primary" type="submit" className="mt-2">
                        Simpan
                    </Button>
                </Form>
            </Modal.Body>
        </Modal>
    )
}

export default ModalComponent
