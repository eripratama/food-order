import React, { Component } from 'react'
import { BrowserRouter, Switch, Route, Link } from "react-router-dom"
import { NavbarComponent } from './components'
import { Home, Finish } from './pages'

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <NavbarComponent />
        <main>
          <Switch>
            <Route path="/" component={Home} exact/>
            <Route path="/finish" component={Finish} exact/>
          </Switch>
        </main>
      </BrowserRouter>
    )
  }
}
